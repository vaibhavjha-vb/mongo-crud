const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017";

MongoClient.connect(url, (err, client) => {
  if (err) throw err;

  const db = client.db("user_management");
  const myQuery = { id: "2" };
  db.collection("users").daleteOne(myQuery, (err, obj) => {
    if (err) throw err;
    console.log(obj.result + " records deleted");
    client.close();
  });
});
