const {MongoClient} = require("mongodb");
const url = "mongodb://localhost:27017/user_management";

MongoClient.connect(url, (err, client) => {
  if (err) throw err;
  const umUsers = [
    {
      id: "1",
      first_name: "Aakash",
      last_name: "Chauhan",
      email: "hsjus@uyf.com",
    },
    {
        id: "2",
        first_name: "Lakshay",
        last_name: "Bhasin",
        email: "laksi@hdj.com",
    },
    {
        id: "3",
        first_name: "Mahira",
        last_name: "Sharma",
        email: "mahira@hihuf.com",
    },
    {
        id: "4",
        first_name: "Priya",
        last_name: "Ahluwalia",
        email: "aaloos@uyf.com",
      },
      {
          id: "5",
          first_name: "Rahul",
          last_name: "Giri",
          email: "girigiri@yuu.com",
      },
  ];
  const db = client.db("user_management");
  db.collection("users").insertMany(umUsers, function (err, res) {
    if (err) throw err;
    console.log("Number of records inserted: " + res.insertedCount);
    client.close();
  });
});
